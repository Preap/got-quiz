let quizData;
let questionNumber = -1;
let answers = [];
let totalPoints = 0;
let currentQuestion = 1;
const PAUSE_DURATION = 3000; //  in ms

$(document).ready(async function () {
    await $.getScript("getQuizData.js");
    await $.getScript("displayNextQuestion.js");
    await getQuizData()
        .then(() => {
            quizData = window.quizData;
            displayNextQuestion();
        })
});


// Change the color of the answer button when pressed
$(".answerButton").click((event) => {
    changeState(event.target);
});


function changeState(theButton) {
    if ($(theButton).hasClass("btn-outline-secondary")) {
        $(theButton).removeClass("btn-outline-secondary");
        $(theButton).removeClass("unselectedAnswer")
        $(theButton).addClass("btn-warning");
        $(theButton).addClass("selectedAnswer");
    } else
        if ($(theButton).hasClass("selectedAnswer")) {
            $(theButton).removeClass("btn-warning");
            $(theButton).removeClass("selectedAnswer");
            $(theButton).addClass("btn-outline-secondary");
            $(theButton).addClass("unselectedAnswer");
        }
}

$("#submit_button").click(async function () {
    selectedAnswersAmount = getAmountOfSelectedAnswers();
    if (selectedAnswersAmount < 1) { // todo
        //Do nothing
    } else {
        await checkAnswer();
        if (currentQuestion === quizData["questions"].length - 1) {
            $("#submit_button").text("SUBMIT FINAL ANSWER!");
        }
        if (currentQuestion < quizData["questions"].length) {
            currentQuestion++;
            displayNextQuestion();
            window.scrollTo(0, 0);
            updateProgressBar();
        } else {
            moveToResultsPage();
        }
    }

})

function moveToResultsPage() {
    // let url = "http://127.0.0.1:5500/results.html?points=" + totalPoints; //FOR LOCAL USE
    let url = "https://lambovgotquiz.netlify.com/results.html?points=" + totalPoints;

    document.location.href = url;
}

function updateProgressBar() {
    progressStep = 100 / quizData["questions"].length;
    width = questionNumber * progressStep;
    width = width + '%';
    $("#progressBar").css("width", width);
}

function getAmountOfSelectedAnswers() {
    let amount = 0;
    $(".selectedAnswer").each(() => {
        amount = amount + 1;
    })
    return amount;
}

async function checkAnswer() {
    let currentQuestion = quizData["questions"][questionNumber];
    let correctAnswer = currentQuestion.correct_answer;
    let questionType = currentQuestion.question_type;
    let points = currentQuestion.points;
    let isCorrect;

    switch (questionType) {
        case "mutiplechoice-single":
            {
                isCorrect = await checkForSingleAnswer(correctAnswer);
                if (isCorrect) {
                    awardPoints(points);
                    await displaySuccess();
                } else { // if answer is wrong
                    await displayErrorForSingleAnswer(correctAnswer);
                }
                break;
            }
        case "truefalse":
            {
                isCorrect = await checkForTrueFalseAnswer(correctAnswer);
                if (isCorrect) {
                    awardPoints(points);
                    await displaySuccess();
                } else { // if answer is wrong
                    await displayErrorsForTrueFalse(correctAnswer);
                }
                break;
            }
        case "mutiplechoice-multiple":
            {
                isCorrect = await checkFormMultipleAnswers(correctAnswer);
                if (isCorrect) {
                    awardPoints(points);
                    await displaySuccess();
                } else { // if answer is wrong
                    await displayErrorsForMultipleAnswers(correctAnswer);
                }
                break;
            }
        default:
            {
                console.error("Non existent question type!");
                displayNextQuestion();
            }
    }
}

function checkForSingleAnswer(correctAnswer) {
    return new Promise((resolve, reject) => {
        let sumOfAnswers = 0;
        let givenAnswer;
        $(".answerButton").each(function () {
            if ($(this).hasClass("selectedAnswer")) {
                givenAnswer = parseInt(this.name);
                sumOfAnswers = sumOfAnswers + 1;
            }
        })
        if (sumOfAnswers != 1) { // if given more that one answer don't award points
            console.log("sumOfAnswers != 1");
            resolve(false);
        }
        if (givenAnswer === correctAnswer) {
            resolve(true);
        }
        resolve(false);
    })
}

function checkForTrueFalseAnswer(correctAnswer) {
    return new Promise((resolve, reject) => {
        let sumOfAnswers = 0;
        let givenAnswer;
        $(".answerButton").each(function () {
            if ($(this).hasClass("selectedAnswer")) {
                givenAnswer = this.textContent.toLowerCase();
                sumOfAnswers = sumOfAnswers + 1;
            }
        })
        if (sumOfAnswers != 1) { // if given more that one answer don't award points
            resolve(false);
        }
        if (correctAnswer.toString() === givenAnswer) {
            resolve(true);
        }
        resolve(false);
    })
}

function checkFormMultipleAnswers(correctAnswer) {
    return new Promise((resolve, reject) => {
        let sumOfAnswers = 0;
        let givenAnswer = [];
        $(".answerButton").each(function () {
            if ($(this).hasClass("selectedAnswer")) {
                givenAnswer.push(parseInt(this.name));
                sumOfAnswers = sumOfAnswers + 1;
            }
        })
        if (sumOfAnswers != correctAnswer.length) {
            console.log("Wrong sum of answers!");
            resolve(false);
        }
        if (arraysMatch(givenAnswer, correctAnswer)) {
            resolve(true);
        }
        resolve(false);
    })
}

function arraysMatch(arr1, arr2) {

    // Check if the arrays are the same length
    if (arr1.length !== arr2.length) return false;

    // Check if all items exist and are in the same order
    for (var i = 0; i < arr1.length; i++) {
        if (arr1[i] !== arr2[i]) return false;
    }

    // Otherwise, return true
    return true;

};

function displaySuccess() {
    return new Promise((resolve, reject) => {
        $(".selectedAnswer").each(function (index, element) {
            element.classList.remove("selectedAnswer");
            element.classList.remove("btn-warning");
            element.classList.add("btn-success");
        });
        setTimeout(() => { resolve() }, PAUSE_DURATION);
    })
}

function displayErrorsForMultipleAnswers(correctAnswer) {
    return new Promise((resolve, reject) => {
        $(".selectedAnswer").each(function (index, element) {
            givenAnswer = parseInt(element.name);
            if (correctAnswer.includes(givenAnswer)) {
                markButtonAsCorrect(element);
            } else {
                markButtonAsWrong(element);
            }
        });
        $(".unselectedAnswer").each(function (index, element) {
            unselectedAnswer = parseInt(element.name);
            if (correctAnswer.includes(unselectedAnswer)) {
                markButtonAsCorrectButNotSelected(element);
            }
        });
        setTimeout(() => { resolve() }, PAUSE_DURATION);
    })
}

function displayErrorForSingleAnswer(correctAnswer) {
    return new Promise((resolve, reject) => {
        $(".selectedAnswer").each(function (index, element) {
            givenAnswer = parseInt(element.name);
            if (givenAnswer === correctAnswer) {
                markButtonAsCorrect(element);
            } else {
                markButtonAsWrong(element);
            }
        });
        $(".unselectedAnswer").each(function (index, element) {
            unselectedAnswer = parseInt(element.name);
            if (unselectedAnswer === correctAnswer) {
                markButtonAsCorrectButNotSelected(element);
            }
        });
        setTimeout(() => { resolve() }, PAUSE_DURATION);
    })

}

function displayErrorsForTrueFalse(correctAnswer) {
    return new Promise((resolve, reject) => {
        $(".selectedAnswer").each(function (index, element) {
            givenAnswer = element.textContent.toLowerCase();
            if (givenAnswer === correctAnswer.toString()) {
                markButtonAsCorrect(element);
            } else {
                markButtonAsWrong(element);
            }
        });
        $(".unselectedAnswer").each(function (index, element) {
            unselectedAnswer = Boolean(element.textContent.toLowerCase());
            if (unselectedAnswer === correctAnswer) {
                markButtonAsCorrectButNotSelected(element);
            }
        });
        setTimeout(() => { resolve() }, PAUSE_DURATION);
    })
}

function awardPoints(points) {
    totalPoints = totalPoints + points;
    console.log("You have a total of " + totalPoints + " points");
}

function markButtonAsCorrect(buttonElement) {
    buttonElement.classList.remove("selectedAnswer");
    buttonElement.classList.remove("btn-warning");
    buttonElement.classList.add("btn-success");
}

function markButtonAsWrong(buttonElement) {
    buttonElement.classList.remove("selectedAnswer");
    buttonElement.classList.remove("btn-warning");
    buttonElement.classList.add("btn-danger");
}

function markButtonAsCorrectButNotSelected(buttonElement) {
    $(buttonElement).css("border", "10px solid  #5cb85c");
    console.log("You should have selected answer " + buttonElement.name);
}