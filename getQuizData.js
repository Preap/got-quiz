function getQuizDataPromise() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "get",
            url: "https://proto.io/en/jobs/candidate-questions/quiz.json",
            dataType: "json",
            success: function (response) {
                console.log("quizData fetched");
                resolve(response);
            },
            error: function () {
                reject("Could not get quiz data from the server");
            }
        });
    }
    );
};

async function getQuizData() {
    await getQuizDataPromise()
        .then((quizData) => {
            window.quizData = quizData;
            console.log("quizData stored for future use");
            return quizData;
        })
        .catch((err) => {
            console.error(err);
        });
}