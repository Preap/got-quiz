function displayNextQuestion() {
    resetAnswerButtons();
    questionNumber++;
    let currentQuestion = quizData["questions"][questionNumber];
    let questionText = currentQuestion.title;
    let imagePath = currentQuestion.img;
    let questionType = currentQuestion.question_type;

    document.getElementById("question_text").textContent = questionText;
    document.getElementById("image").src = imagePath;

    //  Create the answer buttons for the question
    if (questionType == "truefalse") {
        document.getElementById("answer_1").textContent = "TRUE";
        document.getElementById("answer_2").textContent = "FALSE";
        document.getElementById("answer_3").hidden = true;
        document.getElementById("answer_4").hidden = true;
    }
    else { // if not true/false type then create button for every possible answer
        let possibleAnswers = currentQuestion.possible_answers;
        for (i = 0; i < possibleAnswers.length; i++) {
            let index = i + 1;
            document.getElementById('answer_' + index).textContent = possibleAnswers[i].caption;
            document.getElementById('answer_' + index).name = possibleAnswers[i].a_id;
            document.getElementById('answer_' + index).hidden = false;
        }
    }
}

function resetAnswerButtons() {
    $(".answerButton").each(function (index, button) {
        button.classList.remove("btn-danger");
        button.classList.remove("btn-warning");
        button.classList.remove("selectedAnswer");
        button.classList.remove("btn-success");
        button.classList.add("btn-outline-secondary");
        button.classList.add("unselectedAnswer");
        $(button).css("border", "8px solid  #e6e6da");
    });
}