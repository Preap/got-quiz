let totalPoints;
let resultData;

document.onload = start();

async function start() {
    await getResultData();
    await getResultIndex();
    renderResultInformation();
}
$(document).ready(async function () {

});

function getTotalPoints() {
    var url = document.location.href,
        params = url.split('?')[1].split('&'),
        data = {}, tmp;
    for (var i = 0, l = params.length; i < l; i++) {
        tmp = params[i].split('=');
        data[tmp[0]] = tmp[1];
    }
    totalPoints = data.points * 5;
}

function getResultDataPromise() {
    return new Promise((resolve, reject) => {
        $.ajax({
            type: "get",
            url: "https://proto.io/en/jobs/candidate-questions/result.json",
            dataType: "json",
            success: function (response) {
                resolve(response);
            },
            error: function () {
                reject("Could not get quiz data from the server");
            }
        });
    }
    );
};

async function getResultData() {
    await getResultDataPromise()
        .then((quizData) => {
            resultData = quizData;
            console.log("got quizData");
            return quizData;
        })
        .catch((err) => {
            console.error(err);
        });
}

function renderResultInformation() {
    let title = resultData.title;
    let imagePath = resultData.img;
    let message = resultData.message;
    title = title + " You were " + totalPoints + "% correct!";

    document.getElementById("result_title").textContent = title;
    document.getElementById("image").src = imagePath;
    document.getElementById("message").textContent = message;

}

async function getResultIndex() {
    return new Promise((resolve, reject) => {
        let index = 0;
        getTotalPoints();

        for (i = 0; i < resultData["results"].length; i++) {
            if (totalPoints > resultData["results"][i].maxpoints) {
                continue;
            } else {
                index = i;
                break;
            }
        }

        resultData = resultData["results"][index];
        resolve();
    })
}